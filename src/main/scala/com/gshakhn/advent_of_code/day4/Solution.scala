package com.gshakhn.advent_of_code.day4

object Solution {
  def solvePart1(input: String): Int = {
    input.split("\n")
      .filter(validChecksum)
      .map(extractSectorId)
      .sum
  }

  def solvePart2(input: String): Int = {
    input.split("\n")
      .filter(validChecksum)
      .find { encrypted =>
        val decrypted = decrypt(encrypted)
        decrypted.contains("north") && decrypted.contains("pole")
      }
      .map(extractSectorId)
      .get
  }

  def extractSectorId(encrypted: String): Int = {
    Integer.parseInt(encrypted.filter(_.isDigit).mkString).toInt
  }

  def validChecksum(encrypted: String): Boolean = {
    extractChecksum(encrypted) == calculateChecksum(encrypted)
  }

  def extractChecksum(encrypted: String): String = {
    encrypted.reverse.filterNot(_ == ']').takeWhile(_.isLetter).reverse
  }

  def calculateChecksum(encrypted: String): String = {
    val letters = encrypted.filterNot(_ == '-').takeWhile(_.isLetter)
    val countByLetter: Map[Char, Int] = letters.groupBy(identity).mapValues(_.length)
    countByLetter.toSeq.sortWith { case ((l1, c1), (l2, c2)) =>
      if (c1 < c2) {
        false
      } else if (c1 > c2) {
        true
      } else {
        l1 < l2
      }
    }.map(_._1).take(5).mkString("")
  }

  def decrypt(encrypted: String): String = {
    val sectorId = extractSectorId(encrypted)
    encrypted
      .takeWhile(c => c.isLetter || c == '-')
      .dropRight(1)
      .map {
        case c if c.isLetter => (((c.toInt - 'a'.toInt) + sectorId) % 26 + 'a'.toInt).toChar
        case c if c == '-' => ' '
      }
  }
}
