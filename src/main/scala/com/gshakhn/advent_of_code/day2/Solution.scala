package com.gshakhn.advent_of_code.day2

object Solution {
  def solvePart1(input: String): String = {
    val rawKeyPad = "123\n456\n789"
    solve(input, rawKeyPad)
  }

  def solvePart2(input: String): String = {
    val rawKeyPad = "  1  \n 234 \n56789\n ABC \n  D  "
    solve(input, rawKeyPad)
  }

  private def solve(input: String, rawKeyPad: String) = {
    val lines = input.split("\n")
    val result = lines.foldLeft(LineResult("", KeyPad(rawKeyPad))) { (accum, line) =>
      val newKp = line.foldLeft(accum.keyPad) { (kp, command) =>
        kp.move(Direction.toDirection(command))
      }

      LineResult(accum.entrySoFar + newKp.digit, newKp)
    }

    result.entrySoFar
  }

  case class LineResult(entrySoFar: String, keyPad: KeyPad)

  sealed trait Direction
  case object Up extends Direction
  case object Down extends Direction
  case object Left extends Direction
  case object Right extends Direction

  object Direction {
    def toDirection(c: Char): Direction = c match {
      case 'U' => Up
      case 'D' => Down
      case 'L' => Left
      case 'R' => Right
    }
  }

  object KeyPad {
    def apply(rawInput: String): KeyPad = {
      val keys: Array[Array[Char]] = rawInput.split("\n").map(_.toCharArray)
      val fiveY = keys.indexWhere(_.contains('5'))
      val fiveX = keys(fiveY).indexOf('5')
      KeyPad(keys, fiveX, fiveY)
    }
  }

  case class KeyPad(keys: Array[Array[Char]], x: Int, y: Int) {
    private def height = keys.length
    private def width = keys(0).length
    def move(direction: Direction): KeyPad = {
      val (newX, newY) = direction match {
        case Up => (x, y - 1)
        case Down => (x, y + 1)
        case Left => (x - 1, y)
        case Right => (x + 1, y)
      }

      if (newX < 0 || newX == width || newY < 0 || newY == height || keys(newY)(newX) == ' ') {
        this
      } else {
        this.copy(x = newX, y = newY)
      }
    }
    def digit: Char = {
      keys(y)(x)
    }
  }
}
