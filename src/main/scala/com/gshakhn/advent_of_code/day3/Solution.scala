package com.gshakhn.advent_of_code.day3

object Solution {
  def solvePart1(input: String): Int = {
    parsePart1Input(input).count(validTriangle)
  }
  def solvePart2(input: String): Int = {
    parsePart2Input(input).count(validTriangle)
  }

  def parsePart1Input(input: String): Array[Array[Int]] = {
    input.split("\n")
      .map(_.trim)
      .map(_.split(" "))
      .filter(_.length > 0)
      .map(l => l.map(Integer.valueOf).map(_.toInt))
  }

  def parsePart2Input(input: String): Array[Array[Int]] = {
    input.split("\n").grouped(3).flatMap{ threeLines =>
      val numberStrings = threeLines.map(_.split(" ").filter(_.length > 0))
      val numbers = numberStrings
        .map(l => l.map(Integer.valueOf).map(_.toInt))

      Array(Array(numbers(0)(0), numbers(1)(0), numbers(2)(0)),
        Array(numbers(0)(1), numbers(1)(1), numbers(2)(1)),
        Array(numbers(0)(2), numbers(1)(2), numbers(2)(2)))
    }.toArray
  }

  def validTriangle(lengths: Array[Int]): Boolean = {
    /*
    a + b     > c
    a + b + c = s

           -c > c - s
            s > 2c
     */
    val sum = lengths.sum
    lengths.forall(l => l > 0 && 2 * l < sum)
  }
}
