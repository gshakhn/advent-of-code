package com.gshakhn.advent_of_code.day5

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

object Solution {
  def solvePart1(input: String): String = {
    val md5 = MessageDigest.getInstance("md5")
    Stream.from(0)
      .map {
        input + _
      }
      .map(_.getBytes)
      .map(md5.digest)
      .map(DatatypeConverter.printHexBinary)
      .filter(_.startsWith("00000"))
      .map { hex => hex(5) }
      .take(8)
      .mkString
  }

  def solvePart2(input: String): String = {
    val md5 = MessageDigest.getInstance("md5")
    val validHexes = Stream.from(0)
      .map {
        input + _
      }
      .map(_.getBytes)
      .map(md5.digest)
      .map(DatatypeConverter.printHexBinary)
      .filter(_.startsWith("00000"))
      .flatMap(hex => {
        val positionChar = hex(5)
        if (positionChar.isDigit) {
          val digit = Integer.parseInt(positionChar.toString)
          if (digit >= 0 && digit <= 7) {
            val char = hex(6).toLower
            Some((digit, char))
          } else {
            None
          }
        } else {
          None
        }
      }).iterator
    val charsInPassword = decryptComplicated(validHexes, Map.empty)
    Range(0, 8).foldLeft("") { case (password, position) => password + charsInPassword(position) }
  }

  def decryptComplicated(iter: Iterator[(Int, Char)], decodedSoFar: Map[Int, Char]): Map[Int, Char] = {
    if (decodedSoFar.size == 8) {
      decodedSoFar
    } else {
      val positionAndChar = iter.next()
      val position = positionAndChar._1
      if (decodedSoFar.contains(position)) {
        decryptComplicated(iter, decodedSoFar)
      } else {
        val char = positionAndChar._2
        decryptComplicated(iter, decodedSoFar + (position -> char))
      }
    }
  }
}
