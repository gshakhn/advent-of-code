package com.gshakhn.advent_of_code.day1

object Solution {

  def solvePart1(input: String): Int = {
    val end = commands(input).foldLeft(PositionAndOrientation(Position.origin, North)) { (positionAndOrientation, command) =>
      positionAndOrientation.move(command).newPositionAndOrientation
    }

    end.position.distanceFromOrigin
  }

  def solvePart2(input: String): Int = {
    val allMoves = commands(input).foldLeft(Seq(MoveResult(Seq.empty, PositionAndOrientation(Position.origin, North)))) {
      (moveResults, command) =>
        val currentPositionAndOrientation = moveResults.last.newPositionAndOrientation
        val newMove = currentPositionAndOrientation.move(command)
        moveResults :+ newMove
    }

    val allPositionsTraversed = allMoves.flatMap(_.positionsTraversed)

    val searchResults = allPositionsTraversed.foldLeft(SearchResults(None, Set.empty)) { (searchResults, newPosition) =>
      searchResults.hq.map(_ => searchResults).getOrElse {
        val newPositionsSearched = searchResults.positionsSearched + newPosition
        if (searchResults.positionsSearched.contains(newPosition)) {
          SearchResults(Some(newPosition), newPositionsSearched)
        } else {
          SearchResults(None, newPositionsSearched)
        }
      }
    }

    searchResults.hq.get.distanceFromOrigin
  }

  case class SearchResults(hq: Option[Position], positionsSearched: Set[Position])

  private def commands(input: String) = {
    val commands = input.split(", ").map { commandString =>
      val turn = commandString.charAt(0) match {
        case 'R' => Right
        case 'L' => Left
      }
      val steps = commandString.substring(1).toInt
      Command(turn, steps)
    }
    commands
  }

  sealed trait Turn

  case object Right extends Turn

  case object Left extends Turn

  sealed trait Orientation {
    def turn(turn: Turn): Orientation
  }

  case object North extends Orientation {
    override def turn(turn: Turn): Orientation = turn match {
      case Right => East
      case Left => West
    }
  }

  case object East extends Orientation {
    override def turn(turn: Turn): Orientation = turn match {
      case Right => South
      case Left => North
    }
  }

  case object South extends Orientation {
    override def turn(turn: Turn): Orientation = turn match {
      case Right => West
      case Left => East
    }
  }

  case object West extends Orientation {
    override def turn(turn: Turn): Orientation = turn match {
      case Right => North
      case Left => South
    }
  }

  case class Position(x: Int, y: Int) {
    def distanceFromOrigin: Int = Math.abs(x) + Math.abs(y)

    def nextPosition(steps: Int, orientation: Orientation): Position = {
      orientation match {
        case North => Position(x, y + steps)
        case East => Position(x + steps, y)
        case South => Position(x, y - steps)
        case West => Position(x - steps, y)
      }
    }
  }

  object Position {
    def origin: Position = Position(0, 0)
  }

  case class PositionAndOrientation(position: Position, orientation: Orientation) {

    def move(command: Command): MoveResult = {
      val steps = command.steps
      val newOrientation = orientation.turn(command.turn)
      val positionsTraversed = Range(0, steps).map(position.nextPosition(_, newOrientation))
      val newPositionAndOrientation = PositionAndOrientation(position.nextPosition(steps, newOrientation), newOrientation)
      MoveResult(positionsTraversed, newPositionAndOrientation)
    }
  }

  case class MoveResult(positionsTraversed: Seq[Position], newPositionAndOrientation: PositionAndOrientation)

  case class Command(turn: Turn, steps: Int)

}
