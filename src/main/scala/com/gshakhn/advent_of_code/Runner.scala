package com.gshakhn.advent_of_code

import scala.io.Source

object Runner extends App {
    private val inputStream = getClass.getResourceAsStream("/day4/input.txt")
    val input = Source.fromInputStream(inputStream).getLines().mkString("\n")
    println(day4.Solution.solvePart2(input))
}
