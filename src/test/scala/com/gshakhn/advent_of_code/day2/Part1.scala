package com.gshakhn.advent_of_code.day2

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Part1 extends FunSuite with Matchers {
  val tests = Table(
    ("input", "solution"),
    ("U", "2"),
    ("UU", "2"),
    ("D", "8"),
    ("DD", "8"),
    ("L", "4"),
    ("LL", "4"),
    ("R", "6"),
    ("RR", "6"),
    ("UL", "1"),
    ("ULL", "1"),
    ("ULL\nR", "12"),
    ("ULL\nRRDDD", "19"),
    ("ULL\nRRDDD\nLURDL", "198"),
    ("ULL\nRRDDD\nLURDL\nUUUUD", "1985")
  )

  test("day-2") {
    forAll(tests) { (input: String, solution: String) =>
      Solution.solvePart1(input) shouldBe solution
    }
  }
}