package com.gshakhn.advent_of_code.day2

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Part2 extends FunSuite with Matchers {
  val tests = Table(
    ("input", "solution"),
    ("ULL\nRRDDD\nLURDL\nUUUUD", "5DB3")
  )

  test("day-2") {
    forAll(tests) { (input: String, solution: String) =>
      Solution.solvePart2(input) shouldBe solution
    }
  }
}