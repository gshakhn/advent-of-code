package com.gshakhn.advent_of_code.day4

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Test extends FunSuite with Matchers {
  val tests = Table(
    ("input", "calculatedChecksum", "checksum", "sectorId"),
    ("aaaaa-bbb-z-y-x-123[abxyz]", "abxyz", "abxyz", 123),
    ("a-b-c-d-e-f-g-h-987[abcde]", "abcde", "abcde", 987),
    ("not-a-real-room-404[oarel]", "oarel", "oarel", 404)
  )

  test("calculatedChecksum") {
    forAll(tests) { (input: String, calculatedChecksum: String, checkSum: String, sectorId: Int) =>
      Solution.calculateChecksum(input) shouldBe calculatedChecksum
    }
  }

  test("checksum") {
    forAll(tests) { (input: String, calculatedChecksum: String, checkSum: String, sectorId: Int) =>
      Solution.extractChecksum(input) shouldBe checkSum
    }
  }

  test("sectorId") {
    forAll(tests) { (input: String, checksum: String, checkSum: String, sectorId: Int) =>
      Solution.extractSectorId(input) shouldBe sectorId
    }
  }

  test("decrypt") {
    Solution.decrypt("qzmt-zixmtkozy-ivhz-343") shouldBe "very encrypted name"
  }

}