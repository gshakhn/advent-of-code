package com.gshakhn.advent_of_code.day1

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Part1 extends FunSuite with Matchers {
  val tests = Table(
    ("input", "solution"),
    ("R2", 2),
    ("L2", 2),
    ("R2, L3", 5),
    ("R2, R2, R2", 2),
    ("R5, L5, R5, R3", 12)
  )

  test("day-1") {
    forAll(tests) { (input: String, solution: Int) =>
      Solution.solvePart1(input) shouldBe solution
    }
  }
}