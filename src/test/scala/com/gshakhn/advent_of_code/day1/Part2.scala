package com.gshakhn.advent_of_code.day1

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Part2 extends FunSuite with Matchers {
  val tests = Table(
    ("input", "solution"),
    ("R8, R4, R4, R8", 4)
  )

  test("day-1") {
    forAll(tests) { (input: String, solution: Int) =>
      Solution.solvePart2(input) shouldBe solution
    }
  }
}