package com.gshakhn.advent_of_code.day3

import org.scalatest.prop.TableDrivenPropertyChecks.forAll
import org.scalatest.prop.Tables.Table
import org.scalatest.{FunSuite, Matchers}

class Test extends FunSuite with Matchers {
  val tests = Table(
    ("input", "solution"),
    (Array(1, 1, 1), true),
    (Array(1, 2, 2), true),
    (Array(1, 2, 2), true),
    (Array(2, 2, 1), true),
    (Array(1, 1, 2), false),
    (Array(1, 2, 1), false),
    (Array(2, 1, 1), false),
    (Array(-1, -1, -1), false),
    (Array(0, 0, 0), false)

  )

  test("valid-triangle") {
    forAll(tests) { (input: Array[Int], solution: Boolean) =>
      Solution.validTriangle(input) shouldBe solution
    }
  }

  test("parse part1 input") {
    Solution.parsePart1Input("1 2 3\n4 5 6\n7 8 9") shouldBe Array(Array(1,2,3),Array(4,5,6),Array(7,8,9))
  }

  test("parse part2 input") {
    Solution.parsePart2Input("1 2 3\n4 5 6\n7 8 9") shouldBe Array(Array(1,4,7),Array(2,5,8),Array(3,6,9))
  }
}