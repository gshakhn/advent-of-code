scalaVersion := "2.12.1"

organization := "com.gshakhn"

name := "advent_of_code"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-Yno-adapted-args",
  //      "-Ywarn-value-discard", // Adds a bunch of extra noise
  //      "-Ywarn-numeric-widen",
  //      "-Ywarn-dead-code", // confused by ???, sadly
  "-Xlint",
  "-Xfatal-warnings"
)
